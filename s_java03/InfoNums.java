
class InfoNums {
    public static void main(String[] args) {
        float total = args.length;
        float parseMin = Float.parseFloat(args[0]);
        float maximo = 0;
        float minimo = parseMin;
        float sumaTotal = 0;
        for (String s : args){
            float parseS = Float.parseFloat(s);
            if (parseS > maximo) {
                maximo = parseS;
            }
            if (parseS < minimo) {
                minimo = parseS;
            }
            sumaTotal = parseS + sumaTotal;
        }
        float media = (float)sumaTotal/(total -1);
        System.out.println(total + " numeros introducidos");
        System.out.println("Numero mayor: " + maximo);
        System.out.println("Numero menor: " + minimo);
        System.out.println("Media aritmetica: " + media);

    }
}
