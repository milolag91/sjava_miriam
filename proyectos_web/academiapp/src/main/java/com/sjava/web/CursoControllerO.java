package com.sjava.web;

import java.util.ArrayList;
import java.util.List;

public class CursoControllerO {
    private static List<Curso> listaCursos = new ArrayList<Curso>();
    private static int contador = 0;

    static {

        // Alumno a = new Alumno(99,"ricard hernández", "algo@algo.com", "999333");
        // listaAlumnos.add(a);
    }

    // getAll devuelve la lista completa
    public static List<Curso> getAll(){
        return listaCursos;
    }

    //getId devuelve un registro
    public static Curso getId(int id){
        for (Curso c : listaCursos) {
            if (c.getId()==id){
                return c;
            }
        }
        return null;
    }

    public static Curso getProfesor(int profesor){
        for (Curso c : listaCursos) {
            if (c.getProfesor()== profesor){
                return c;
            }
        }
        return null;
    }

    //save guarda un alumno
    // si es nuevo (id==0) lo añade a la lista
    // si ya existe, actualiza los cambios
    public static void save(Curso curso) {
        if (curso.getId() == 0){
            contador++;
            curso.setId(contador);
            listaCursos.add(curso);
        } else {
            for (Curso c : listaCursos) {
                if (c.getId()==c.getId()) {

                    c.setNombre(curso.getNombre());
                    c.setDuracion(curso.getDuracion());
                    c.setProfesor(curso.getProfesor());
                    break;
                }
            }
        }
        
    }
    // removeId elimina alumno por id
    public static void removeId(int id){
        Curso borrar=null;
        for (Curso c : listaCursos) {
            if (c.getId()==id){
                borrar = c;
                break;
            }
        }
        if (borrar!=null) {
            listaCursos.remove(borrar);
        }
    }

}