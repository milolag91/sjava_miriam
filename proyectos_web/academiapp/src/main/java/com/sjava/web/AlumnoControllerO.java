package com.sjava.web;

import java.util.ArrayList;
import java.util.List;

public class AlumnoControllerO {
    private static List<Alumno> listaAlumnos = new ArrayList<Alumno>();
    private static int contador = 0;

    static {

        // Alumno a = new Alumno(99,"ricard hernández", "algo@algo.com", "999333");
        // listaAlumnos.add(a);
    }

    // getAll devuelve la lista completa
    public static List<Alumno> getAll(){
        return listaAlumnos;
    }

    //getId devuelve un registro
    public static Alumno getId(int id){
        for (Alumno p : listaAlumnos) {
            if (p.getId()==id){
                return p;
            }
        }
        return null;
    }

    //save guarda un alumno
    // si es nuevo (id==0) lo añade a la lista
    // si ya existe, actualiza los cambios
    public static void save(Alumno al) {
        if (al.getId() == 0){
            contador++;
            al.setId(contador);
            listaAlumnos.add(al);
        } else {
            for (Alumno ali : listaAlumnos) {
                if (ali.getId()==al.getId()) {

                    ali.setNombre(al.getNombre());
                    ali.setEmail(al.getEmail());
                    ali.setTelefono(al.getTelefono());
                    break;
                }
            }
        }
        
    }

    // size devuelve numero de alumnos
    public static int size() {
        return listaAlumnos.size();
    }


    // removeId elimina alumno por id
    public static void removeId(int id){
        Alumno borrar=null;
        for (Alumno p : listaAlumnos) {
            if (p.getId()==id){
                borrar = p;
                break;
            }
        }
        if (borrar!=null) {
            listaAlumnos.remove(borrar);
        }
    }

}