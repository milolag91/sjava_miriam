package com.sjava.web;

import java.util.ArrayList;
import java.util.List;

public class ProfesorControllerO {
    private static List<Profesor> listaProfesores = new ArrayList<Profesor>();
    private static int contador = 0;

    static {

        // Alumno a = new Alumno(99,"ricard hernández", "algo@algo.com", "999333");
        // listaAlumnos.add(a);
    }

    // getAll devuelve la lista completa
    public static List<Profesor> getAll(){
        return listaProfesores;
    }

    //getId devuelve un registro
    public static Profesor getId(int id){
        for (Profesor p : listaProfesores) {
            if (p.getId()==id){
                return p;
            }
        }
        return null;
    }

    //save guarda un alumno
    // si es nuevo (id==0) lo añade a la lista
    // si ya existe, actualiza los cambios
    public static void save(Profesor p) {
        if (p.getId() == 0){
            contador++;
            p.setId(contador);
            listaProfesores.add(p);
        } else {
            for (Profesor prof : listaProfesores) {
                if (prof.getId() == prof.getId()) {

                    prof.setNombre(p.getNombre());
                    prof.setEmail(p.getEmail());
                    prof.setTelefono(p.getTelefono());
                    prof.setEspecialidad(p.getEspecialidad());
                    break;
                }
            }
        }
        
    }

    // size devuelve numero de alumnos
    public static int size() {
        return listaProfesores.size();
    }


    // removeId elimina alumno por id
    public static void removeId(int id){
        Profesor borrar=null;
        for (Profesor p : listaProfesores) {
            if (p.getId()==id){
                borrar = p;
                break;
            }
        }
        if (borrar!=null) {
            listaProfesores.remove(borrar);
        }
    }

}