package com.sjava.web;

import java.util.ArrayList;
import java.util.List;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import com.mysql.jdbc.Connection;

/* datos en base de datos */

public class CursoController {

    // constantes utilizadas en las ordenes sql
	private static final String TABLE = "cursos";
	private static final String KEY = "idcursos";


    // getAll devuelve todos los registros de la tabla
    public static List<Curso> getAll(){
        
        List<Curso> listaCursos = new ArrayList<Curso>();
		String sql = String.format("select %s,nombre,horas,profesores_idprofesores from %s", KEY, TABLE);
		//String sql = "select id,nombre,password from "+TABLE;
		
		try (Connection conn = DBConn.getConn();
				Statement stmt = conn.createStatement()) {

			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				Curso u = new Curso(
                    (Integer) rs.getObject(1),
                    (String) rs.getObject(2),
                    (Integer) rs.getObject(3),
                    (Integer) rs.getObject(4));
				listaCursos.add(u);
			}
		} catch (Exception e) {
			String s = e.getMessage();
			System.out.println(s);
		}
        return listaCursos;

    }

    //getId devuelve un registro
    public static Curso getId(int id){
        Curso u = null;
        String sql = String.format("select %s,nombre,horas,profesores_idprofesores from %s where %s=%d", KEY, TABLE, KEY, id);
        try (Connection conn = DBConn.getConn();
                Statement stmt = conn.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                u = new Curso(
                (Integer) rs.getObject(1),
                (String) rs.getObject(2),
                (Integer) rs.getObject(3),
                (Integer) rs.getObject(4));
            }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return u;
    }

    //save guarda un alumno
    // si es nuevo (id==0) lo añade a la BDD
    // si ya existe, actualiza los cambios
    public static void save(Curso c) {
        String sql;
        if (c.getId()>0) {
            sql = String.format("UPDATE %s set nombre=?, horas=?, profesores_idprofesores=? where %s=%d", TABLE, KEY, c.getId());
        }else {
            sql = String.format("INSERT INTO %s (nombre, horas, profesores_idprofesores) VALUES (?,?,?)", TABLE);
        }
        try (Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql);
                Statement stmt = conn.createStatement()) {
            pstmt.setString(1, c.getNombre());
            pstmt.setInt(2, c.getDuracion());
            pstmt.setInt(3, c.getProfesor());
            pstmt.executeUpdate();
        if (c.getId()==0) {
            //usuario nuevo, actualizamos el ID con el recién insertado
            ResultSet rs = stmt.executeQuery("select last_insert_id()");
                if (rs.next()) {
                    c.setId(rs.getInt(1));
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    // size devuelve numero de alumnos
    public static int size() {
        return 0;
    }


    // removeId elimina alumno por id
    public static void removeId(int id){
        String sql = String.format("DELETE FROM %s where %s=%d", TABLE, KEY, id);
        try (Connection conn = DBConn.getConn();
                Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(sql);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}