package com.sjava.web;

public class Curso {

    private int id;
    private String nombre;
    private int duracion;
    private int profesor;

    public Curso(String nombre, int duracion) {
        this.nombre = nombre;
        this.duracion = duracion;
        // cambio! no se añade de forma automática, se obliga a ejecutar el método "guarda" de AlumnoController
        // AlumnoController.nuevoContacto(this);
    }

    public Curso(String nombre, int duracion, int profesor) {
        this.nombre = nombre;
        this.duracion = duracion;
        this.profesor = profesor;
        // cambio! no se añade de forma automática, se obliga a ejecutar el método "guarda" de AlumnoController
        // AlumnoController.nuevoContacto(this);
    }

    public Curso(int id, String nombre, int duracion) {
        this.id = id;
        this.nombre = nombre;
        this.duracion = duracion;
    }

    public Curso(int id, String nombre, int duracion, int profesor) {
        this.id = id;
        this.nombre = nombre;
        this.duracion = duracion;
        this.profesor = profesor;
    }

    public String getNombre() {
        return this.nombre;
    }

    protected void setNombre(String nombre){
        this.nombre = nombre;
    }

    public int getDuracion(){
        return this.duracion;
    }

    protected void setDuracion(int duracion){
        this.duracion = duracion;
    }

    public int getId(){
        return this.id;
    }

    protected void setId(int id){
        this.id=id;
    }

    public int getProfesor() {
        return this.profesor;
    }

    public void setProfesor(int profesor){
        this.profesor = profesor;
    }
}