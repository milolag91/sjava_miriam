package com.sjava.testmoto;

class Moto {
    String marca;
    String modelo;
    int cc;
    int cv;
    int pvp;

    public Moto(String marca, String modelo, int cc, int cv, int pvp) {
        this.marca = marca;
        this.modelo = modelo;
        this.cc = cc;
        this.cv = cv;
        this.pvp = pvp;
    }

    public void comparaCv(Moto otra){
        if (this.cv > otra.cv) {
            System.out.printf(
                "La %s %s es más potente que la %s %s\n",
                this.marca, this.modelo, otra.marca, otra.modelo);
        } else {
            System.out.printf(
                "La %s %s es más potente que la %s %s\n",
                otra.marca, otra.modelo, this.marca, this.modelo);   
        }
    }

    public void comparaPrecio(Moto otra) {
        if(this.pvp > otra.pvp){
            int precioDif = this.pvp - otra.pvp;
            System.out.println("La " + this.marca +" vale " + precioDif + " mas cara que la " + otra.marca);
        }
        if(this.pvp < otra.pvp){
            int precioDif2 = otra.pvp - this.pvp;
            System.out.println("La " + otra.marca +" vale " + precioDif2 + "mas cara que la " + this.marca);
        }
    }
}
