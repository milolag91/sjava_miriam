import java.util.List;
import java.util.Map;
import java.util.TreeMap;

class java_test2 {

    public static void main(String[] args) {
        String[] motos = {"honda", "yamaha", "kawasaki", "sanglas", "norton"};

        List<String> listaMotos = new ArrayList<String>();
        Set<String> listaSinDuplicados = new TreeSet<>(); // Con el HashSet no se ordenan alfabéticamente.


        for(String marca : motos) {
            listaMotos.add(marca);
            listaSinDuplicados.add(marca);
        }

        String marca1 = listaMotos.get(0);
        
        for(String marca : listaSinDuplicados) {
            System.out.println(marca);
        }

        System.out.println("Total marcas: " + listaMotos.size());
        System.out.println("Total marcas treelist: " + listaMotos.size());
    }
}