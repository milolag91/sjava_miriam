package com.sjava.utilidades;

public class Datos{

    public static String[] getDias(String idioma){
        if (idioma.equals("es")) {
            return new String[] {"lunes", "martes", "miércoles", "jueves", "viernes", "sábado", "domingo" };
        }
        else {
            return new String[] {"dilluns", "dimarts", "dimecres", "dijous", "divendres", "dissabte", "diumenge" };
        }
        
    }
}
