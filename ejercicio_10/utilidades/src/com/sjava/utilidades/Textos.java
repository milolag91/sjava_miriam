package com.sjava.utilidades;
import java.text.Normalizer;

public class Textos {

    public static String capitaliza(String in) {
        return in.substring(0,1).toUpperCase() + in.substring(1).toLowerCase();
    }

    public static String slug(String frase) {
        String fraseSlug = frase.replaceAll(" ", "_");
        fraseSlug = fraseSlug.toLowerCase();
        fraseSlug = Normalizer.normalize(fraseSlug, Normalizer.Form.NFD)
        .replaceAll("[^\\p{ASCII}]", "");
        return fraseSlug;
    }
}