
class Person {
    String nombre;
    int edad;

    public void comparaEdad(Person persona) {
        if (this.edad > persona.edad) {
            System.out.println(this.nombre + " es mayor que " + persona.nombre);
        }else if (persona.edad > this.edad){
            System.out.println(persona.nombre + " es mayor que " + this.nombre); 
        } else {
            System.out.println(this.nombre + " tiene la misma edad que " + persona.nombre);
        }
    }
}

