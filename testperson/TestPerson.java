
class TestPerson {
    public static void main(String[] args) {
        Person p1 = new Person();
        p1.nombre = "Ana";
        p1.edad = 22;

        Person p2 = new Person();
        p2.nombre = "Antonio";
        p2.edad = 22;

        System.out.println("Nombre: " + p1.nombre);
        System.out.println("Edad: " + p1.edad);
        p1.comparaEdad(p2);
    }
}
