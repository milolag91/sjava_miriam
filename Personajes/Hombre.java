class Hombre extends Persona {

    public Hombre (String nombre, String sexo) {
        super(nombre, sexo);
    }
    
    public void setNombre(String nombre) {
        super.setNombre(nombre);
    }
    @Override
    public String getNombre() {
        return ("el Sr. " + nombre);
    }
    
    public void setEdad(int edad) {
        super.setEdad(edad);
    }
    @Override
    public int getEdad() {
        return edad;
    }
    
    public void setSexo(String sexo) {
        super.setSexo(sexo);
    }
    @Override
    public String getSexo() {
        return sexo;
    }
}