class Persona {
    protected String nombre;
    protected int edad;
    protected String sexo;

    protected Persona (String nombre, String sexo) {
        this.nombre = nombre;
        this.sexo = sexo;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public int getEdad() {
        return edad;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getSexo() {
        return sexo;
    }

    public void presentacion() {
        System.out.println("Soy " + getNombre() + " y tengo " + getEdad() + " años.");
    }
}