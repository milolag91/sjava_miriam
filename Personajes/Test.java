import java.util.Scanner;

class Test {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);

        
        System.out.printf("Nombre? ");
        String nombre = keyboard.nextLine();
        System.out.printf("Edad? ");
        int edad = keyboard.nextInt();
        String sobra = keyboard.nextLine();
        System.out.printf("H/M? ");
        String sexo = keyboard.nextLine();
        
        
        System.out.println();

        if (sexo.equals("H")) {
            Persona hombre1 = new Hombre(nombre, sexo);
            hombre1.setEdad(edad);
            hombre1.presentacion();
        } else {
            Persona mujer1 = new Mujer(nombre, sexo);
            mujer1.setEdad(edad);
            mujer1.presentacion();
        }
    }
}