class Mujer extends Persona {
    public Mujer (String nombre, String sexo){
        super(nombre, sexo);
    }

    @Override
    public void setNombre(String nombre) {
        super.setNombre(nombre);
    }

    public String getNombre() {
        return ("la Sra. " + nombre);
    }

    public void setEdad(int edad1) {
        if (edad1 >= 45) {
            edad = (edad1 - 5);
        }else {
            super.setEdad(edad1);
        }
    }

    public int getEdad() {
        return edad;
    }

    public void setSexo(String sexo) {
        super.setSexo(sexo);
    }

    public String getSexo() {
        return sexo;
    }
}