class Animalitos {
    public static void main(String[] args) {
        
        Animal pajaro1 = new Pajaro();
        Animal perro1 = new Perro();
        Animal serpiente1 = new Serpiente();
        Animal perroMutante1 = new PerroMutante();

        System.out.println("Pajaro:");
        pajaro1.come();
        pajaro1.duerme();
        ((Pajaro)pajaro1).vuela();
        System.out.println(pajaro1.getNumeroPatas());
        System.out.println();

        System.out.println("Perro:");
        perro1.come();
        perro1.duerme();
        ((Perro)perro1).ladra();
        System.out.println(perro1.getNumeroPatas());
        System.out.println();

        System.out.println("Serpiente:");
        serpiente1.come();
        serpiente1.duerme();
        ((Serpiente)serpiente1).repta();
        System.out.println(serpiente1.getNumeroPatas());

        System.out.println("Perro mutante:");
        perro1.come();
        perro1.duerme();
        ((PerroMutante)perroMutante1).ladra();
        System.out.println(perroMutante1.getNumeroPatas());
        System.out.println();
    }
}