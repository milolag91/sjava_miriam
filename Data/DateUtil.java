import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.text.SimpleDateFormat;

public class DateUtil {

    private DateUtil() {}

    public static Date createDate(int year, int month, int day) {
    Calendar date = Calendar.getInstance();
    date.set(year, (month -1), day);
    
    return date.getTime();
    }

    public static Date createDate(int year, int month, int day, int hour, int min) {
        Calendar date = Calendar.getInstance();
        date.set(year, (month -1), day, hour, min);
        
        return date.getTime();
        }
    
    public static String formatDate(Date date, String pattern) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        
        return sdf.format(date);
    }

    public static void getSunday(int year, int month) {
        int actualMonth = month -1;
        Calendar cal = Calendar.getInstance();
        cal.set(year, actualMonth, 1);

        while(cal.get(Calendar.MONTH) == actualMonth) {
            if(cal.get(Calendar.DAY_OF_WEEK) == 1) {
                System.out.println(cal.getTime());
            }
            cal.add(Calendar.DATE, 1);
        }

    }

}