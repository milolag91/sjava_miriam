import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class DateUtilTest {

    public static void main(String[] args) { 

        Date data = DateUtil.createDate(2018, 06, 07);
        System.out.println(data);

        StringBuilder sb = new StringBuilder();
        sb.append("La fecha es ");
        sb.append(DateUtil.formatDate(data, "d"));
        sb.append(" de ");
        sb.append(DateUtil.formatDate(data, "MMMM"));
        sb.append(" de ");
        sb.append(DateUtil.formatDate(data, "yyyy"));
        System.out.println(sb.toString());

        DateUtil.getSunday(2018, 6);
    }
}