import java.util.Scanner;

class palindromos {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);

        System.out.printf("Inserta una palabra: ");
        String palabra = keyboard.nextLine();

        char[] charsPal = palabra.toCharArray();
        String palin = "";

        for(int i = charsPal.length -1; i >= 0; i-- ) {
            palin += charsPal[i];
        }
        String toLower = palin.toLowerCase();
        String toUpper = toLower.substring(0, 1).toUpperCase() + toLower.substring(1);

        System.out.printf("Palindromo: " + toUpper);
    }
}