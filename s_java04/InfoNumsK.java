import java.util.Scanner;

import jdk.nashorn.internal.ir.Flags;

class InfoNumsK {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        int total = 0;
        int num=0;
        boolean bolEx = false;
        int maximo = 0;
        int minimo = 0;
        boolean primero = true;
        do {
        System.out.printf("Entra un num: ");
        bolEx = false;
        try {
        num = keyboard.nextInt();
        if (primero) {
            maximo = num;
            minimo = num;
            primero = false;
        }
        total += 1;
        if (num > maximo) {
            maximo = num;
        }
        if (num < minimo && num != 0) {
            minimo = num;
        }
            } catch (Exception e) {
        System.out.println("***Dato incorrecto - 0 para salir***");
        bolEx = true;
        keyboard.next();
        }
        } while (num !=0 || bolEx);
        System.out.println("Has introducido "+ (total -1) + " numeros");
        System.out.println("Maximo: " + maximo);
        System.out.println("Minimo: " + minimo);
        keyboard.close();
        }
}