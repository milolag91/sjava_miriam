import java.util.Random;
import java.util.Scanner;

class Adivina {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        Random random = new Random();
        int incognita = random.nextInt(10)+1;
        int num = 0;
        int total = 0;
        do {
            System.out.printf("Adivina del 1 al 10: ");
            try {
            num = keyboard.nextInt();
            total += 1;

            if (num < 1 || num > 10) {
                System.out.println("***Numero fuera de limites***");
            }else {
                System.out.println("Ese no es!");
            }
                } catch (Exception e) {
            System.out.println("***Dato incorrecto***");
            keyboard.next();
            }
            } while (num != incognita);
            System.out.println("Has acertado! El numero es "+ incognita);
            System.out.println("El numero de intentos ha sido " + total);
            keyboard.close();
            }
    }