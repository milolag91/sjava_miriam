import java.text.Collator;
import java.text.Normalizer;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

class provincias {
    public static void main(String[] args) {
        
        SortedSet<String> provincias = new TreeSet<String>();
        Datos datos = new Datos();
    
        for(String lista : datos.provincias()){ 
            lista = Normalizer.normalize(lista, Normalizer.Form.NFD);
            lista = lista.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
            provincias.add(lista);
        }
        
        Iterator it = provincias.iterator();
        while (it.hasNext()) {
        System.out.println(it.next());
        }
    }
}