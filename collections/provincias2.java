import java.text.Collator;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.SortedSet;
import java.util.TreeSet;

//https://picodotdev.github.io/blog-bitix/2017/11/ordenar-alfabeticamente-cadenas-con-la-clase-collator-en-java/

class provincias2 {
    public static void main(String[] args) {
        
        SortedSet<String> provincias = new TreeSet<String>();
        Datos datos = new Datos();
        List<Object> provinciasAcentos = new ArrayList<Object>();
        Collator primaryCollator = Collator.getInstance(new Locale("es"));
        primaryCollator.setStrength(Collator.PRIMARY);
    
        for(String lista : datos.provincias()){ 
            provincias.add(lista);
        }
        
        Iterator it = provincias.iterator();
        while (it.hasNext()) {
        provinciasAcentos.add(it.next());
        }

        provinciasAcentos.sort(primaryCollator);
        for(int i = 0; i <= provinciasAcentos.size(); i++) {
            System.out.println(provinciasAcentos.get(i));
        }
    }
}