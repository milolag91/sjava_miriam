
package com.sjava;
import java.util.ArrayList;

public class PersonaController {
    private static ArrayList<Persona> contactos = new ArrayList<Persona>();
    private static int contador = 0;

    public static void muestraContactos(){
        for (Persona p : contactos) {
            System.out.println(p);
        }
    }

    public static void muestraContactoId(int id) {
        for(Persona p : contactos) {
            if (p.getId() == id) {
                System.out.println(p);
                return;
            }
        }
    }
    
    public static void eliminaContacto(int id) {
        for(Persona p : contactos) {
            if (p.getId() == id) {
                contactos.remove(p);
                return;
            }
        }
    }
    
    public static ArrayList<Persona> getContactos() {
        return contactos;
    }

    public static void nuevoContacto(Persona pers) {
        contador++;
        pers.setId(contador);
        contactos.add(pers);   
    }

    public static int numContactos() {
        return contactos.size();
    }
}
